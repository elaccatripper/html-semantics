import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgileTeamComponent } from './components/agile-team/agile-team.component';
import { HowWeWorkComponent } from './components/how-we-work/how-we-work.component';

const routes: Routes = [
  {
    path: '',
    component: HowWeWorkComponent,
  },
  {
    path: 'agile-team',
    component: AgileTeamComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
