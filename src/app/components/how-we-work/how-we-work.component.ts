import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { filter, map, Observable } from 'rxjs';

const contentIdMap: Map<string, string> = new Map([
  [
    '1',
    'A helyszínen felmérjük a megoldandó üzleti problémát, és az agilis termékfejlesztési módszertan szerint elkészítjük az első elemzéseket, koncepció szintű terveket.',
  ],
  [
    '2',
    'Szakmai csapatával közösen elkészítjük a konzultációs lépésben azonosított célokat kiszolgáló priorizált feladatlistát',
  ],
  [
    '3',
    'A felhasználói élmény tervezéssel foglalkozó csapatunk a felhasználókkal készített interjúk alapján elkészíti a szoftver grafikai terveit, prototípusát.',
  ],
  ['4', 'Kéthetente működő funkciókat prezentálunk.'],
]);

@Component({
  selector: 'app-how-we-work',
  templateUrl: './how-we-work.component.html',
  styleUrls: ['./how-we-work.component.scss'],
})
export class HowWeWorkComponent implements OnInit {
  public form = new FormGroup({
    radioInfo: new FormControl<string | null>(null),
  });
  public selectedRadioInfo$: Observable<string | undefined> | undefined;

  ngOnInit(): void {
    this.selectedRadioInfo$ = this.form.get('radioInfo')?.valueChanges.pipe(
      filter(Boolean),
      map((value) => contentIdMap.get(value))
    );
  }
}
