import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgileTeamOnPushComponent } from './agile-team-on-push.component';

describe('AgileTeamOnPushComponent', () => {
  let component: AgileTeamOnPushComponent;
  let fixture: ComponentFixture<AgileTeamOnPushComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AgileTeamOnPushComponent]
    });
    fixture = TestBed.createComponent(AgileTeamOnPushComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
