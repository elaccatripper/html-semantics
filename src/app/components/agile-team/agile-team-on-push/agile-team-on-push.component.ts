import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { toSignal } from '@angular/core/rxjs-interop';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-agile-team-on-push',
  templateUrl: './agile-team-on-push.component.html',
  styleUrls: ['./agile-team-on-push.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AgileTeamOnPushComponent implements OnInit {
  public signalForm = new FormGroup({
    name: new FormControl<string>(''),
  });

  public defaultForm = new FormGroup({
    name: new FormControl<string>(''),
  });

  public nameSignal = toSignal(this.signalForm.get('name')!.valueChanges, { initialValue: '' });
  public nameObs$: Observable<string | null> | undefined;

  ngOnInit() {
    this.nameObs$ = this.defaultForm.get('name')?.valueChanges;
  }
}
