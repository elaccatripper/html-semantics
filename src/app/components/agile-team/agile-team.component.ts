import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { map, Observable } from 'rxjs';

@Component({
  selector: 'app-agile-team',
  templateUrl: './agile-team.component.html',
  styleUrls: ['./agile-team.component.scss'],
})
export class AgileTeamComponent implements OnInit {
  public form = new FormGroup({
    componentType: new FormControl<string | null>(null),
  });

  public selectedComponent$: Observable<string> | undefined;

  ngOnInit(): void {
    this.selectedComponent$ = this.form.get('componentType')?.valueChanges.pipe(
      map((id) => {
        return id === '1' ? 'default' : 'onPush';
      })
    );
  }
}
