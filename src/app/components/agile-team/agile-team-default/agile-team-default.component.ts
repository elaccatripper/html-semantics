import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { toSignal } from '@angular/core/rxjs-interop';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-agile-team-default',
  templateUrl: './agile-team-default.component.html',
  styleUrls: ['./agile-team-default.component.scss'],
})
export class AgileTeamDefaultComponent {
  public signalForm = new FormGroup({
    name: new FormControl<string>(''),
  });

  public defaultForm = new FormGroup({
    name: new FormControl<string>(''),
  });

  public buttonForm = new FormGroup({
    exampleText: new FormControl<string>(''),
  });

  public nameSignal = toSignal(this.signalForm.get('name')!.valueChanges, { initialValue: '' });
  public nameObs$: Observable<string | null> | undefined = this.defaultForm.get('name')?.valueChanges;

  public fillForm(): void {
    this.buttonForm.controls.exampleText.setValue('Nitrowise');
  }

  public clearForm(): void {
    this.buttonForm.reset();
  }
}
