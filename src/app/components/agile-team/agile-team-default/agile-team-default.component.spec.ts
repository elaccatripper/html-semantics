import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { AgileTeamDefaultComponent } from './agile-team-default.component';
import { MockModule } from 'ng-mocks';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { By } from '@angular/platform-browser';

describe('AgileTeamDefaultComponent', () => {
  let component: AgileTeamDefaultComponent;
  let fixture: ComponentFixture<AgileTeamDefaultComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AgileTeamDefaultComponent, MockModule(TranslateModule)],
      imports: [ReactiveFormsModule],
    });
    fixture = TestBed.createComponent(AgileTeamDefaultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display text on button click', fakeAsync(() => {
    fixture.debugElement.query(By.css('#fillBtn')).nativeElement.click();

    tick();

    let inputValue = fixture.debugElement.query(By.css('#exampleText')).nativeElement.value;

    expect(inputValue).toBe('Nitrowise');
  }));
});
